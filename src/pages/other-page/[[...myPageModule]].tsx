import type {NextPage} from 'next';
import Head from 'next/head';
import styles from '../../../styles/Home.module.css';
import Box from "@mui/material/Box";
import React, {useEffect, useState} from "react";
import { useRouter } from 'next/router'

import Grid from "@mui/material/Grid";
import AboutUs from "../../components/AboutUs";

import PageLayout from "../../components/PageLayout";

const TradeMainPanel: NextPage = () => {
    //console.log("page called");
    const router = useRouter();
    let  pageModule = router.query.myPageModule;
    if (pageModule != null) {
        //console.log("module :", pageModule[0]);
        pageModule = pageModule[0]
    }



    useEffect(() => {
        // Remove the server-side injected CSS.
        const jssStyles: any = document.querySelector("#jss-server-side");
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
    }, []);

    return (
        <PageLayout modulePath={pageModule} >
            <Box className={styles.container}>
                <Head>
                    <title>MY CUSTOM TITLE</title>
                    <link rel="icon" href="/favicon.ico"/>
                </Head>
                <Box className={styles.main}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            { (pageModule === "dashboard" || !pageModule) && <h3> this is dashboard</h3> }
                            { (pageModule === "about") && <AboutUs /> }
                            { (pageModule === "settings") && <p> no settings !</p>}
                        </Grid>

                    </Grid>
                </Box>
            </Box>

        </PageLayout>
    );
};

export default TradeMainPanel;


