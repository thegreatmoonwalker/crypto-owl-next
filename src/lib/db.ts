import mysql from 'mysql';


const DB : any  = {
    pfx  : "mw_",
    con  : null,
    tbl_name : "",
    connected : false,
    last_id : 0,
    connect : function (){
        return new Promise((resolve  , reject) => {
            if(!this.connected){
                this.con = mysql.createConnection({
                    host: "localhost",
                    user: "dbuser",
                    database : "mydatabase",
                    password: "#############",
                    charset : "UTF8MB4_UNICODE_CI" // utf8mb4_unicode_ci
                });
                this.con.connect((err: any) => {
                    if (err) throw err;
                    this.connected = true;
                    console.log("DB Connected!");
                    resolve("Connected!");
                    //cb();
                });
            }else{
                resolve("Already Connected!");
            }
        });

    },
    close : function(){
        this.con.end();
    },
    escape : function(input : any){
        //if(this.con){
            return this.con.escape(input) as string;
        //}else{
            //return this.addslashes(input) as string;
        //}

    },
    addslashes : ( str : string ) => {
        return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
    },
    query : async function(sql : string){
        console.info("got sql ------ " , sql);
        await this.connect();
        //console.log(sql);
        return new Promise((resolve , reject) => {
            try {
                this.con.query(sql, function (err : any, result : any) {
                    if (err) throw err;
                    //console.log("got result" , result)

                    /*result :

                    {
                        fieldCount: 0,
                        affectedRows: 14,
                        insertId: 0,
                        serverStatus: 2,
                        warningCount: 0,
                        message: '\'Records:14  Duplicated: 0  Warnings: 0',
                        protocol41: true,
                        changedRows: 0
                    }
                    */


                    resolve (result);
                    //console.log("Result: " + result);
                });
            }catch(err){
                reject(err);
            }

        })

    },
    row : async function(tblName : string, where  = "", col = "*"   ){
        let query : string ;
        if (where){
            query = "SELECT " + col + " FROM " + this.pfx + tblName + " WHERE " + where;
        }else{
            query = "SELECT " + col + " FROM " + this.pfx + tblName;
        }

        if (col != "*") {
            const row : any = await this.query(query);
            const cols_size : any = col.split(",").length;
            if(cols_size > 1){
                return row[0];
            }else{
                return row[0][col];
            }
        } else {
            const row : any = await this.query(query);

            return row[0] ? row[0] : {}
        }

    },
    rows : async function(tblName : string, where = "", col  = "*"   ){

        let query : string ;
        if (where){
            query = "SELECT " + col + " FROM " + this.pfx + tblName + " WHERE " + where;
        }else{
            query = "SELECT " + col + " FROM " + this.pfx + tblName;
        }
        return await this.query(query);
    },
    set : function(tblName : string){
        this.tbl_name = tblName;
    },
    get : function(tblName : string){
        return this.pfx + tblName;
    },
    insert : async function(data : any){
        await this.connect();
        const dt_vals : any = [];
        Object.keys(data).forEach((item : string , index : number) => {
            dt_vals.push(this.escape(data[item] ) as string );
        })
        let query = "INSERT INTO " + this.pfx + this.tbl_name;
        query += ' ( ' + Object.keys(data).join(",") + ' ) VALUES ';
        query += " ( " + dt_vals.join(" , ") + " ) ";
        const res = await this.query(query);

        this.last_id = res.insertId ?  res.insertId : 0;
        return res;
    },
    update : async function(data : any , where = "") {
        await this.connect();
        let query: string = "UPDATE " + this.pfx + this.tbl_name + " SET ";
        const pairs: any = [];

        Object.keys(data).forEach((item: string, index: number) => {
            //dt_vals.push();
            pairs.push(item + "=" + this.escape(data[item]));
        })
        //$str = substr($str, 0, strlen($str) - 1);
        if (where) {
            query = query + pairs.join(",") + ' WHERE ' + where;
        } else {
            query = query + pairs.join(",");
        }

        return await this.query(query);
    },
    delete : async function( where : string){

        if(!where){
            return false;
        }
        return await this.query("DELETE FROM " + this.pfx + this.tbl_name + " WHERE " + where);
    },
    fetch_all : async function (where = '', ordering = '', offset = '', length = '')
    {
        await this.connect();
        if (!this.tbl_name) {
            console.log("No Table Selected");
            return false;
        }

        const str_limit = (length !== '' && offset !== '') ? " LIMIT " + this.escape(offset) + " ," + (length) : "";
        const str_order = ordering ? " ORDER BY " + this.escape(ordering) : "";
        let query : string ;
        if (where) {
            query = "SELECT * FROM " + this.pfx + this.tbl_name + " WHERE " + where + str_order + str_limit;
        } else{
            query = "SELECT * FROM " + this.pfx + this.tbl_name + str_order + str_limit;
        }


        return await this.query(query);

    }


}

export default DB;
