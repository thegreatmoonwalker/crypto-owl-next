import {styled} from "@mui/material/styles";


export default function Logo({size = "mini"} : { size : string}){
    //mini - thumb - large
    const CustomLogo = styled('img')(({theme}) => ({
        height: '30px',
        marginLeft : '1rem'

    }));
    let imgsrc : string = "";
    if(size == "mini"){
        imgsrc = "/assets/logo-typo.png";
    }
    else if(size == "thumb") {
        imgsrc = "/assets/logo.png";
    }
    else if(size == "large") {
        imgsrc = "/assets/logo.png";
    }
    return (
        <CustomLogo src={imgsrc} />
    )
}