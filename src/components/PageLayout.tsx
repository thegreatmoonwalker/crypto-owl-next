import {AppBar, Button, IconButton, Typography} from "@mui/material";
import Toolbar from "@mui/material/Toolbar";
import MenuIcon from "@mui/icons-material/Menu";
import styles from '../../styles/Home.module.css';
import Box from "@mui/material/Box";
import Head from "next/head";
import MenuBottomNavigation from './bottomNav';
import * as React from "react";
import Link from "next/link";
import {useRouter} from "next/router";
//import {useEffect} from "react";
//import Footer from './footer'
export default function PageLayout({ children , modulePath="" } : {children : any , modulePath : any}) {

    const router = useRouter();

    const onChangeRoute = (path : string) => {
        if(path==='dashboard'){
            path = ""
        }
        router.push('/other-page/'+path, undefined, { shallow: true })
    }
   /* useEffect(() => {

    } , [q]);*/
    return (
        <>
            <Head>
                <title>My Page Layout</title>
                <meta name="description" content="Dev : @thegreatmoonwalker"/>
                <link rel="icon" href="/favicon.ico"/>
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0" />
                <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
            </Head>
            <AppBar position="sticky">
                <Toolbar>
                    <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{mr: 2}}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{flexGrow: 1}}>
                        MY Base Project
                    </Typography>
                    <Link href="/"><Button color="inherit">Button</Button></Link>

                </Toolbar>
            </AppBar>
            <Box className={styles.container}>{children}</Box>
            <MenuBottomNavigation page={modulePath} changeRoute={onChangeRoute} />
            {/*<Footer />*/}
        </>
    )
}