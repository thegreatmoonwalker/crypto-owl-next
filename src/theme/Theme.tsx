import React from "react";

import {ThemeProvider , createTheme} from "@mui/material/styles";


const theme = createTheme({
    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: '#121213',
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
            main: '#ee4c7d',
            // dark: will be calculated from palette.secondary.main,
        },
        error: {
            main : '#d32f2f'
        },
        warning : {
            main : '#ED6C02'
        },
        info : {
            main : '#0288d1'
        },
        success : {
            main : '#2e7d32'
        },
        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
    },
});

export default function Theme({children}){
    return (

            <ThemeProvider theme={theme}>
                    {children}
            </ThemeProvider>


    )
}